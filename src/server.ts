import express from 'express';
import fs from 'fs';
import path from 'path'
import https from 'https';
import dotenv from 'dotenv';
import { client, createTables } from './database';
import session from 'express-session';
import {RecaptchaV2} from 'express-recaptcha'
import jwt from 'jsonwebtoken'
import * as crypto from 'crypto-js'

declare module 'express-session' {
  interface SessionData {
    results: any; 
    token: any;
    attempts: number;
    alert: string;
    email: any;
    password: any;
  }
}

dotenv.config()

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;
const app = express();

const siteKey = '6LevNwgpAAAAACDv2CZr4X6XMCUuEVIC2CFFdGsU'
const secretKey = '6LevNwgpAAAAAJk-Ac_vnz95ZcrxT0yMMlHpRLGY'
const recaptcha = new RecaptchaV2(siteKey, secretKey)


createTables()
app.use(session({
  secret: 'secret', 
  resave: true,
  saveUninitialized: true
}));

app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'pug');
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  req.session.attempts = undefined;
  req.session.attempts = undefined;
  req.session.results = undefined;
  res.render('home')
});

app.get('/sql', (req, res) => {
  const rows = req.session.results;
  res.render('sql', {rows})
});

app.post('/', async (req, res) => {
  req.session.results = undefined;
  const query = `SELECT * FROM "person" WHERE email = '${req.body.email}' AND password = '${req.body.pass}'`
  // ' OR 1=1;--
  try{
    const result = await client.query(query)
    req.session.results = result.rows;
    res.redirect('/sql')
  } catch(err){
    res.redirect('/err')
  }
  
})

app.get('/err', (req, res) => {
  res.render('err')
})

app.get('/badAuth', (req, res) => {
  req.session.token = undefined;
  if(!req.session.attempts) {
    req.session.attempts = 0
  }
  var alertMess = undefined;
  if(req.session.alert){
    alertMess = req.session.alert;
    req.session.alert = undefined
  }
  var attempts = req.session.attempts
  res.render('auth', {siteKey, attempts, alertMess})
});

app.post('/login', recaptcha.middleware.verify, async(req, res) => {
  if(req.recaptcha && !req.recaptcha.error) {
    const email = req.body.email;
    const password = req.body.pass
    try{
      const result = await client.query(`SELECT * FROM "login" WHERE email = '${email}'`)
      let rows = result.rows[0];
      var hash = crypto.SHA256(password + rows.salt).toString()
      var pass = rows.password_hash
      if(pass == hash) {
        const token = jwt.sign({email}, 'tajni_kljuc', {expiresIn: '1h'})
        req.session.token = token;
        res.redirect('/protected')
      } else {
        if(!req.session.attempts){
          req.session.attempts = 0;
        }
        req.session.attempts+=1
        req.session.alert = 'User not found'
        res.redirect('/badAuth')
      }
    } catch(err){
      if(!req.session.attempts){
        req.session.attempts = 0;
      }
      req.session.attempts+=1
      req.session.alert = 'User not found'
      res.redirect('/badAuth')

    }

  } else {
    if(!req.body.enable) {
      req.session.attempts = undefined
      const email = req.body.email;
      const password = req.body.pass
      try{
        const result = await client.query(`SELECT * FROM "login" WHERE email = '${email}'`)
        let rows = result.rows[0];
        var hash = crypto.SHA256(password + rows.salt).toString()
        var pass = rows.password_hash
        if(pass == hash) {
          req.session.email = email;
          req.session.password = password;
          res.redirect('/unprotected')
        } else {
          req.session.alert = 'Password is wrong'
          res.redirect('/badAuth')
        }
      
      }catch(err) {
        req.session.alert = 'Email is wrong'
        res.redirect('/badAuth')
      }
    }
    if(req.body.enable) {
      if(!req.session.attempts){
        req.session.attempts = 0;
      }
      req.session.attempts+=1
      req.session.alert = 'Something went wrong'
      res.redirect('/badAuth')
    }
  }
})

app.get('/protected', (req, res) => {
  const token = req.session.token;
  if(!token) {
    res.render('err')
  }

  jwt.verify(token, 'tajni_kljuc', async(err: any, decoded: any) => {
    if(err) {
      res.render('err')
    }

    const email = decoded.email
    const result = await client.query(`SELECT * FROM "login" WHERE email = '${email}'`)
    var row = result.rows[0];
    const protect = true;
    res.render('protected', {row, protect})
  })
  
})

app.get('/unprotected', async(req,res)=>{
  const email= req.session.email;
  const password = req.session.password;
  const result = await client.query(`SELECT * FROM "login" WHERE email = '${email}'`)
  var row = result.rows[0];
  
  const protect = false;
  res.render('protected', {row, protect, password})
})

if(externalUrl){
  const hostname = '0.0.0.0'; //ne 127.0.0.1
  app.listen(port, hostname, () => {
    console.log(`Server locally running at http://${hostname}:${port}/ and from
    outside on ${externalUrl}`);
  });
}
else{
  https.createServer({
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.cert')
  }, app)
  .listen(port, function () {
  console.log(`Server running at https://localhost:${port}/`);
  });
}


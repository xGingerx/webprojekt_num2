import * as crypto from 'crypto-js';
const cry = require('crypto')

const {Client} = require('pg')

export const client = new Client({
    host:process.env.DB_HOST,
    user:process.env.DB_USER,
    port:process.env.DB_PORT,
    password:process.env.DB_PASS,
    database:process.env.DB_DATABASE
})

const createPerson = 
`CREATE TABLE IF NOT EXISTS "person"(
    "email" TEXT,
    "password" TEXT NOT NULL,
    "name" TEXT,
    PRIMARY KEY("email")
)`

const addToTable =
`INSERT INTO "person" VALUES 
    ('ivan@gmail.com', 'ivan123', 'Ivan'),
    ('borna@gmail.com', 'borna123', 'Borna'),
    ('marko@gmail.com', 'marko123', 'Marko'),
    ('ana@gmail.com', 'ana123', 'Ana')
`

const checkTableEmpty = `SELECT COUNT(*) AS num FROM "person"`


export const createTables = async () => {
    await client.connect();
    try {
        await client.query(createPerson)
        const result = await client.query(checkTableEmpty)
        const numRows = result.rows[0].num
        if(numRows == 0) {
            await client.query(addToTable)
        }

        await client.query(CreateLoginTable)
        const res2 = await client.query(`SELECT * FROM "login"`)
        if(res2.rowCount < 1) {
            await client.query(AddToLoginTable)
        }
        
    } catch(err) {
        console.log(err)
    }   
    
}

const CreateLoginTable = 
    `CREATE TABLE IF NOT EXISTS "login" (
        "email" TEXT,
        "password_hash" TEXT NOT NULL,
        "salt" TEXT NOT NULL,
        "name" TEXT,
        PRIMARY KEY("email")
    )`

const email = 'ivan@gmail.com'
const salt = cry.randomBytes(16).toString('base64')
const password = 'I2va54n#' + salt
const hash = crypto.SHA256(password).toString()



const AddToLoginTable =
    `INSERT INTO "login" VALUES
    ('${email}', '${hash}', '${salt}', 'Ivan')`


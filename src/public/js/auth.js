document.addEventListener('DOMContentLoaded', function() {
    const alertMess = window.alertMess;

    if(alertMess) {
        alert(alertMess)
    }

    const checkbox = document.getElementById('enable');
    const recaptcha = document.getElementById('recaptcha')
    checkbox.addEventListener('change', ()=> {
        if(checkbox.checked) {
            recaptcha.style.display = 'block'
            
        } else {
            recaptcha.style.display = 'none'
        }
    })
})
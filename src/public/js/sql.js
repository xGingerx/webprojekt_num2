function validateForm(){
    const enabled = document.getElementById('enable')
    const email = document.getElementById('email').value
    const pass = document.getElementById('pass').value

    if(enabled.checked) {
        if (!email.match(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/)) {
            alert("You entered incorrect input.");
            return false;
        }
        if (!pass.match(/^[a-zA-Z0-9_-]+$/)) {
            alert("You entered incorrect input.");
            return false;
        }
    } 
    return true;
}